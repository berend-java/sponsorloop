package main;

import java.util.ArrayList;

public class Deelnemer {
	private String naam;
	private ArrayList<Sponsoring> sponseringen = new ArrayList<Sponsoring>();
	private static ArrayList<Deelnemer> deelnemers = new ArrayList<Deelnemer>();
	
	public Deelnemer(String n) {
		naam = n;
		deelnemers.add(this);
	}
	
	public String getNaam() {
		return naam;
	}
	
	public ArrayList<Sponsoring> getSponseringen() {
		return sponseringen;
	}
	
	public static ArrayList<Deelnemer> getDeelnemers() {
		return deelnemers;
	}
	
	public static Deelnemer getDeelnemerbByName(String nm) {
		Deelnemer dn = null;
		for(Deelnemer d: deelnemers) {
			if(d.getNaam().equals(nm)) {
				dn = d;
			}
		}
		return dn;
	}
	
	public static String[] getDeelnemersString() {
		String tem = "";
		for(Deelnemer d: deelnemers) {
			tem+= d.getNaam() + " ";
		}
		String[] dd = tem.split(" ");
		return dd;
	}
}
