package main;

import java.util.ArrayList;

public class Sponsor {
	private String naam;
	private static ArrayList<Sponsor> sponsors = new ArrayList<Sponsor>();
	
	public Sponsor(String n) {
		naam =n;
		sponsors.add(this);
	}
	
	public String getNaam() {
		return naam;
	}

	public static ArrayList<Sponsor> getSponsors() {
		return sponsors;
	}
	
	public static Sponsor getSponsorbByName(String nm) {
		Sponsor sl = null;
		for(Sponsor s: sponsors) {
			if(s.getNaam().equals(nm)) {
				sl = s;
			}
		}
		return sl;
	}
	
	public static String[] getDeelnemersString() {
		String tem = "";
		for(Sponsor s: sponsors) {
			tem+= s.getNaam() + " ";
		}
		String[] ss = tem.split(" ");
		return ss;
	}
}
