package main;

import java.awt.*;
import java.awt.event.*;

import javax.swing.*;

public class Gui extends JFrame implements ActionListener {
	private JButton sponsorloop, sponsor, deelnemer, sponsoring;
	private JTextField dNaam, sNaam, s2Naam;
	private JLabel dNaamLabel, sNaamLabel, s2NaamLabel;
	private JButton dAdd, sAdd, s2Add;
	private int lastButton = 0;
	
	public Gui() {
		sponsorloop = new JButton("Nieuwe sponsorloop");
		sponsorloop.addActionListener(this);
		deelnemer = new JButton("Nieuwe Deelnemer");
		deelnemer.addActionListener(this);
		sponsor = new JButton("Nieuwe Sponsor");
		sponsor.addActionListener(this);
		sponsoring = new JButton("Nieuwe Sponsoring");
		sponsoring.addActionListener(this);
		
		dAdd = new JButton("Create");
		dAdd.addActionListener(this);
		dNaamLabel = new JLabel("Naam: ");
		dNaam = new JTextField(10);
		
		sAdd = new JButton("Create");
		sAdd.addActionListener(this);
		sNaamLabel = new JLabel("Naam: ");
		sNaam = new JTextField(10);
		
		s2Add = new JButton("Create");
		s2Add.addActionListener(this);
		s2NaamLabel = new JLabel("Naam: ");
		s2Naam = new JTextField(10);
		
		add(sponsorloop);
		add(deelnemer);
		add(sponsor);
		add(sponsoring);
		
		setSize(598, 300);
		setVisible(true);
		setDefaultCloseOperation(EXIT_ON_CLOSE);
		setLayout(new GridLayout(0,4));
		
		pack();
	}

	public void actionPerformed(ActionEvent e) {
		if(e.getSource() == deelnemer) {
			lastButton = 0;
			fixOthers(lastButton);
			add(dNaamLabel);
			add(dNaam);
			add(dAdd);
			enable(deelnemer);
		} 
		else if (e.getSource() == sponsor) {
			lastButton = 1;
			fixOthers(lastButton);
			add(sNaamLabel);
			add(sNaam);
			add(sAdd);
			enable(sponsor);
		} 
		else if (e.getSource() == sponsorloop) {
			lastButton = 2;
			fixOthers(lastButton);
			add(s2NaamLabel);
			add(s2Naam);
			add(s2Add);
			enable(sponsorloop);
		} 
		else if (e.getSource() == sponsoring) {
			lastButton = 3;
			fixOthers(lastButton);
			JFrame gui2 = new Gui2();
		} 
		else if (e.getSource() == dAdd) {
			System.out.println(dNaam.getText());
			new Deelnemer(dNaam.getText());
			System.out.println("Deelnemer object gemaakt");
		} 
		else if (e.getSource() == sAdd) {
			System.out.println(sNaam.getText());
			new Sponsor(sNaam.getText());
			System.out.println("Sponsor object gemaakt");
		} 
		else if (e.getSource() == s2Add) {
			System.out.println(s2Naam.getText());
			new Sponsorloop(s2Naam.getText());
			System.out.println("Sponsorloop object gemaakt");
		} 
	}

	public void fixOthers(int i) {
		switch(i) {
			case 0: // Deelnemer
				sponsor.setEnabled(true); 
				sponsorloop.setEnabled(true); 
				sponsoring.setEnabled(true); 
				removeSponsor();
				removeSponsorloop();
			break;
			
			case 1: // Sponsor
				deelnemer.setEnabled(true); 
				sponsorloop.setEnabled(true); 
				sponsoring.setEnabled(true); 
				removeDeelnemer();
				removeSponsorloop();
			break;
				
			case 2: // Sponsorloop
				sponsor.setEnabled(true); 
				deelnemer.setEnabled(true); 
				sponsoring.setEnabled(true); 
				removeDeelnemer();
				removeSponsorloop();
				removeSponsor();
		break;
		}
	}
	
	public void removeDeelnemer() {
		remove(dAdd); 
		remove(dNaam); 
		remove(dNaamLabel);
	}
	public void removeSponsor() {
		remove(sAdd);
		remove(sNaam); 
		remove(sNaamLabel);
	}
	public void removeSponsorloop() {
		remove(s2Add);
		remove(s2Naam); 
		remove(s2NaamLabel);
	}
	public void enable(JButton b) {
		b.setEnabled(false);
		setVisible(true);
		this.pack();
	}
}