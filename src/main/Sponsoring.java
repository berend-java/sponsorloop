package main;

public class Sponsoring {
	private Sponsor sponsor;
	private int bedrag;
	private String eis;
	private Sponsorloop sponsorloop;
	
	public Sponsoring(Sponsor s, Deelnemer l, Sponsorloop sl, int b, String e) {
		sponsor = s;
		bedrag = b;
		eis = e;
		sponsorloop = sl;
	}
	
	public Sponsor getSponsor() {
		return sponsor;
	}
	
	public void setSponsor(Sponsor sponsor) {
		this.sponsor = sponsor;
	}

	public int getBedrag() {
		return bedrag;
	}
	
	public void setBedrag(int bedrag) {
		this.bedrag = bedrag;
	}
	
	public String getEis() {
		return eis;
	}
	
	public void setEis(String eis) {
		this.eis = eis;
	}
	public Sponsorloop getSponsorloop() {
		return sponsorloop;
	}
	
	public void setSponsorloop(Sponsorloop sponsorloop) {
		this.sponsorloop = sponsorloop;
	}
}
