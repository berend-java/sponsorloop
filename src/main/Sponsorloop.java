package main;

import java.util.ArrayList;

public class Sponsorloop {
	private String naam;
	private ArrayList<Deelnemer> lopers = new ArrayList<Deelnemer>();
	private ArrayList<Sponsor> sponsors = new ArrayList<Sponsor>();
	private static ArrayList<Sponsorloop> sponsorlopen = new ArrayList<Sponsorloop>();
	
	public Sponsorloop(String n) {
		naam = n;
		sponsorlopen.add(this);
	}
	
	public String getNaam() {
		return naam;
	}
	
	public void addSponsor(Sponsor nS) {
		boolean add = true;
		for(Sponsor s: sponsors) {
			if(s.getNaam() == nS.getNaam()) {
				add = false;
			}
		}
		if(add || sponsors.size() == 0) {
			sponsors.add(nS);
		}
	}
	
	public void addDeelnemer(Deelnemer nL) {
		boolean add = true;
		for(Deelnemer l: lopers) {
			if(l.getNaam() == nL.getNaam()) {
				add = false;
			}
		} 
		if(add || sponsors.size() == 0) {
			lopers.add(nL);
		}
	}
	
	public ArrayList<Deelnemer> getDeelnemers() {
		return lopers;
	}
	
	public ArrayList<Sponsor> getSponsors() {
		return sponsors;
	}

	public static ArrayList<Sponsorloop> getSponsorlopen() {
		return sponsorlopen;
	}
	
	public static Sponsorloop getSponsorloopbByName(String nm) {
		Sponsorloop sl = null;
		for(Sponsorloop s: sponsorlopen) {
			if(s.getNaam().equals(nm)) {
				sl = s;
			}
		}
		return sl;
	}
	
	public static String[] getSponsorloopString() {
		String tem = "";
		for(Sponsorloop s: sponsorlopen) {
			tem+= s.getNaam() + " ";
		}
		String[] dd = tem.split(" ");
		return dd;
	}
}
