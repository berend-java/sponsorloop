package main;

import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JTextField;

public class Gui2 extends JFrame implements ActionListener {
	private JTextField s3Naam, s3Afspraak, s3Bedrag;
	private JLabel s3NaamLabel, s3Label3, s3Label4, s3Label5, s3Label6, s3Label7;
	private JButton s3Add;
	private JComboBox deelnemers, sponsors, sponsorlopen;
	
	public Gui2() {
		s3Add = new JButton("Create");
		s3Add.addActionListener(this);
		s3Naam = new JTextField(10);
		s3Afspraak = new JTextField(20);
		s3Bedrag = new JTextField(20);
		
		s3NaamLabel = new JLabel("Naam: ");
		s3Label3 =  new JLabel("Select Deelnemer");
		s3Label4 =  new JLabel("Select Sponsor");
		s3Label5 = new JLabel("Afspraak");
		s3Label6 = new JLabel("Sponsorloop");
		s3Label7 = new JLabel("Bedrag:");
		
		deelnemers = new JComboBox(Deelnemer.getDeelnemersString());
		deelnemers.addActionListener(this);
		sponsors = new JComboBox(Sponsor.getDeelnemersString());
		sponsors.addActionListener(this);
		
		sponsorlopen = new JComboBox(Sponsorloop.getSponsorloopString());
		sponsorlopen.addActionListener(this);
		
		add(s3NaamLabel);
		add(s3Naam);
		add(s3Label3);
		add(deelnemers);
		add(s3Label4);
		add(sponsors);
		add(s3Label6);
		add(sponsorlopen);
		add(s3Label5);
		add(s3Afspraak);
		add(s3Label7);
		add(s3Bedrag);
		add(s3Add);
		
		setSize(598, 300);
		setVisible(true);
		setDefaultCloseOperation(HIDE_ON_CLOSE);
		setLayout(new GridLayout(0,2));
		
		pack();
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		if (e.getSource() == s3Add) {
			if(!sponsors.getSelectedItem().equals("") && !deelnemers.getSelectedItem().equals("") && !sponsorlopen.getSelectedItem().equals("") && !s3Afspraak.getText().equals("")) {
				Sponsor s = Sponsor.getSponsorbByName((String) sponsors.getSelectedItem());
				Deelnemer d = Deelnemer.getDeelnemerbByName((String) deelnemers.getSelectedItem());
				Sponsorloop ss = Sponsorloop.getSponsorloopbByName((String) sponsorlopen.getSelectedItem());
				new Sponsoring(s, d, ss, Integer.parseInt(s3Bedrag.getText()), s3Afspraak.getText()); 
				System.out.println("Sponsoring object gemaakt");
			}
		}
	}
}
